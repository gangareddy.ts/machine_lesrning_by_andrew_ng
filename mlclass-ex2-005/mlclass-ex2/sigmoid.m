function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).

for i = 1:size(z,2)
	disp(i);
	for j = 1:length(z)
		a = -1.*z(j,i);
		g(j,i) = 1./(1 + exp(a));
	end
% =============================================================
end
